<?php

abstract class Config {

    const SITENAME = "testsite";
    const ADDRESS = "http://testsite";


    const DB_HOST = "localhost";
    const DB_USER = "root";
    const DB_PASSWORD = "root";
    const DB_NAME = "catcen.local";
    const DB_PREFIX = "xyz_";

    const DIR_IMG = "/images/";
    const DIR_TMPL = "/home/testsite/catcen.test/www/tmpl/";


    const FORMAT_DATE = "%d.%m.%Y %H:%M:%S";

    const COUNT_ARTICLES_ON_PAGE = 12;
    const COUNT_SHOW_PAGES = 10;


    const SEF_SUFFIX = ".html";

}
