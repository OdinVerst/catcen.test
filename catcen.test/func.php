<?php
require_once('config.inc.php');

function db_connect () {
    global $mysql_host, $mysql_user, $mysql_pass,$mysql_dbName;
    $l = mysqli_connect($mysql_host, $mysql_user, $mysql_pass,$mysql_dbName);
    if (!$l) {
        echo "Error: no connect to MySQL." . PHP_EOL;
        echo "Cod number error: " . mysqli_connect_errno() . PHP_EOL;
        echo "Text error: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }

    echo "Connect success!!!" . PHP_EOL;
    echo "Info to server: " . mysqli_get_host_info($l) . PHP_EOL;

    mysqli_close($l);
}

db_connect();